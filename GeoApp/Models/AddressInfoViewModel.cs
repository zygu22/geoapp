﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoApp.Models
{
    public class AddressInfoViewModel
    {
        public AddressInfoViewModel()
        {
            addressList = new List<AddressInfo>();
        }

        public List<AddressInfo> addressList { get; set; }
        public string Address { set; get; }
        public string Ip { set; get; }
    }
}