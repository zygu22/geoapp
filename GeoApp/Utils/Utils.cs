﻿using GeoApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace GeoApp.Utils
{
    static class Utils
    {
        public static string GetJason(string ip)
        {
            string jsonString;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(string.Format("http://api.ipstack.com/" + ip + "?access_key=f56832e227330a401549cfe8402c3a13"));
                webRequest.Method = "GET";
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                
                using (Stream stream = webResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }

                return jsonString;
            }
            catch (WebException)
            {
                return jsonString = string.Empty;
            }
        }
    }
}