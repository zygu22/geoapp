﻿using GeoApp.Data;
using GeoApp.Models;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GeoApp.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext _dbContext;

        public HomeController()
        {
            _dbContext = new AppDbContext("Server=tcp:pmikolajczyk.database.windows.net,1433;Initial Catalog=GeoApp;Persist Security Info=False;User ID=PMikolajczyk22;Password=Geoapp2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        public ActionResult Index()
        {
            var ViewModel = new AddressInfoViewModel();
            try
            {
                ViewModel.addressList = _dbContext.AddressInfos.ToList();
            }
            catch(SqlException)
            {
                return View(ViewModel);
            }

            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Index(List<AddressInfo> addressList, AddressInfoViewModel model, string command)
        {
            var ViewModel = new AddressInfoViewModel();

            try
            {
                ViewModel.addressList = _dbContext.AddressInfos.ToList();
                var ip = model.Address;

                string jsonString = Utils.Utils.GetJason(ip);
                var desrializedJson = JsonConvert.DeserializeObject<dynamic>(jsonString);

                ViewModel.Ip = desrializedJson.ip;

                if (command == "Check data")
                {
                    if (_dbContext.AddressInfos.Any(o => o.Ip == ViewModel.Ip))
                    {
                        return View("Check", ViewModel);
                    }
                    else
                    {
                        AddRecord(desrializedJson);
                        ViewModel.addressList = _dbContext.AddressInfos.ToList();
                        return View("Check", ViewModel);
                    }
                }
                else if (command == "Save data")
                {
                    if (_dbContext.AddressInfos.Any(o => o.Ip == ViewModel.Ip))
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        AddRecord(desrializedJson);
                    }
                }
                else
                {
                    AddressInfo addressInfo = new AddressInfo();
                    addressInfo.Ip = desrializedJson.ip;
                    var address = (from AddressInfo in _dbContext.AddressInfos where AddressInfo.Ip == addressInfo.Ip select AddressInfo).Single();
                    _dbContext.AddressInfos.Remove(address);
                    _dbContext.SaveChanges();
                }
            }
            catch (SqlException)
            {
                return RedirectToAction("Index");
            }
            catch (RuntimeBinderException)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        private void AddRecord(dynamic desrializedJson)
        {
            AddressInfo addressInfo = new AddressInfo();

            addressInfo.Ip = desrializedJson.ip;
            addressInfo.Continent = desrializedJson.continent_name;
            addressInfo.Country = desrializedJson.country_name;
            addressInfo.Region = desrializedJson.region_name;
            addressInfo.City = desrializedJson.city;
            addressInfo.Zip = desrializedJson.zip;
            addressInfo.Latitude = desrializedJson.latitude;
            addressInfo.Longitude = desrializedJson.longitude;

            _dbContext.AddressInfos.Add(addressInfo);
            _dbContext.SaveChanges();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}